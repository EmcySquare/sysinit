#!/bin/bash
###############################################################################
#
# BASIC SYSTEM SETUP SCRIPT
#
###############################################################################

INSTDIR=$(pwd)

RESP=$(mktemp)
trap "rm -f $RESP" 0 1 2 5 15

###############################################################################
# Split functions

function apt_up {
	echo "Updating system..."
	apt-get update
	apt-get --install-suggests --fix-missing --quiet --yes upgreade
}


function install_packages {
	PACKLIST=$1
	echo "Installing $PACKLIST mandatory packages..."
	cd $INSTDIR/packages
	if [[ ! -e $PACKLIST ]]; then
		return
	fi
	for P in $(cat $PACKLIST)
	do
		apt-get --fix-missing --quiet --yes --force-yes install $P
	done
	mv $PACKLIST "$PACKLIST"_done
}


function setup_ntp {
	cd $INSTDIR
	dialog --ascii-lines --title NTP --clear --yesno "Install NTP Cron job?" 5 40
	case $? in
	  0)
        	cat > /etc/cron.daily/ntpdate << EOF
#!/bin/bash
ntpdate 0.it.pool.ntp.org ntp1.inrim.it
EOF
		chmod +x /etc/cron.daily/ntpdate
		dialog --ascii-lines --title NTP --infobox "Please wait as I set up your clock..." 4 40
		timedatectl set-timezone Europe/Rome
		/etc/cron.daily/ntpdate 
        	service cron restart
		;;
	  *)
	     	return;;
	esac
}


function setup_hostname {
	cd $INSTDIR
	dialog --ascii-lines --title Hostname --clear --inputbox "Give a hostname to this box" 6 60 $(cat /etc/hostname) 2>$RESP
	if [[ "$?" != "0" ]]; then
                return
        fi

	if [ -s $RESP ]; then
		cat $RESP > /etc/hostname
		hostname -F /etc/hostname
	fi
}


function setup_dns {
	cd $INSTDIR
	dialog  --ascii-lines --title "DNS setup" --clear --yesno "Modify default DNS server?" 5 40
	if [[ "$?" != "0" ]]; then
		return
	fi

	dialog  --ascii-lines --title "DNS setup" --clear --inputbox "Insert DNS servers as x.x.x.x, y.y.y.y;" 8 60 "208.67.222.222, 208.67.220.220;" 2>$RESP
	
	echo "prepend domain-name-servers $(cat $RESP)" >> /etc/dhcp/dhclient.conf
}



function setup_ssh_server {
	cd $INSTDIR
	dialog --ascii-lines --title SSH --clear --yesno "Setup SSH server? " 5 40
	if [[ "$?" != "0" ]]; then
		return
	fi
	
	cat ssh/sshd_config > /etc/ssh/sshd_config
	service ssh restart
}

function setup_ssh_key {
	cd $INSTDIR
        dialog --ascii-lines --title SSH --clear --yesno "Setup SSH key? " 5 40
	if [[ "$?" != "0" ]]; then
		return
	fi

	cd ssh/dot_ssh/
	cat authorized_keys >> /root/.ssh/authorized_keys
	if [[ -f /root/.ssh/id_rsa.pub ]]; then
		dialog --ascii-lines --defaultno --title SSH --clear --yesno "An ssh-key-pair seems to be already
there in /root/.ssh folder

Do you whant to overwrite it? " 12  60
	
		if [[ "$?" != "0" ]]; then
			return
		fi
	fi
	rm -f /root/.ssh/id_rsa*
	cat id_rsa.pub >> /root/.ssh/id_rsa.pub
	chown -R root:root /root/.ssh
	chmod -R go-rwx /root/.ssh
}


function add_users {
	cd $INSTDIR
	dialog --ascii-lines --title "Add Users" --clear --yesno "Add non-privileged users? " 5 40
        if [[ "$?" != "0" ]]; then
                return
        fi

	## Add Users emcy & oint
	adduser --disabled-password emcy
	adduser --disabled-password oint
	adduser --disabled-password git
	## oint can use passwords since in ssh_config it's IP restricted
	echo "emcy:emcysquare" | chpasswd
	echo "oint:oint" | chpasswd
	echo "git:version1" | chpasswd

	## Emcy can only access via ssh key exchange
	cd ssh/emcy_ssh/
	mkdir /home/emcy/.ssh
	cp * /home/emcy/.ssh/.
	chown -R emcy:emcy /home/emcy/.ssh
	chmod -R go-rwx /home/emcy/.ssh

        cd ../git_ssh/
        mkdir /home/git/.ssh
        cp * /home/git/.ssh/.
        chown -R git:git /home/git/.ssh
        chmod -R go-rwx /home/git/.ssh

	## Make both sudoers
	adduser emcy sudo
	adduser oint sudo
}



function setup_iptables {
	cd $INSTDIR
	dialog --ascii-lines --title IP-Tables --clear --yesno "Setup iptables firewall? " 5 40
        if [[ "$?" != "0" ]]; then
                return
        fi
	
	cd $INSTDIR/firewall
	dialog --ascii-lines --title IP-Tables --backtitle "Select install dir" --clear --dselect /root 20 40 2> $RESP
        if [[ "$?" != "0" ]]; then
	        return
        fi
	IPTDIR=$(cat $RESP)
	cp ip*.sh $IPTDIR

	cat /etc/rc.local | grep -v "exit 0" | grep -v "ipscript.sh" > $RESP
	echo "$IPTDIR/ipscript.sh" >> $RESP
	echo "exit 0" >> $RESP
	cat $RESP > /etc/rc.local

	dialog --ascii-lines --backtitle "Edit iptables script to your needs" --title "ipscript.sh" \
		--editbox $IPTDIR/ipscript.sh 80 80 2> $RESP
	cat $RESP > $IPTDIR/ipscript.sh


	dialog --ascii-lines --title IP-Tables --defaultno --clear --yesno "Start iptables firewall NOW? " 5 40
        if [[ "$?" != "0" ]]; then
                return
        fi
	$IPTDIR/ipscript.sh	
}



function setup_apache {
	install_packages apache
	a2enmod rewrite
	a2enmod ssl
	service apache2 restart
}


function setup_mysql {
	install_packages mysql
	
	 mysql << EOF
CREATE USER 'backup'@'localhost' IDENTIFIED BY 'backup';
GRANT SELECT, SHOW VIEW, RELOAD, REPLICATION CLIENT, EVENT, TRIGGER ON *.* TO 'backup'@'localhost';
flush privileges;
EOF

}


function setup_mail {
	install_packages mail
}


function setup_net {
	install_packages network
	cd $INSTDIR
	cp pdnsd/pdnsd.conf /etc
	service pdnsd restart	
}

function setup_extra {
        install_packages extra
}


function config_functions {
	dialog --ascii-lines --backtitle "Select functions for this box" \
		--title "Select function(s)" \
		--checklist "What's this box gonna be and do? \n" 20 60 10 \
"Apache"	"Apache webserver" off \
"MySQL"		"MySQL Database Server" off \
"Mail"		"Mail server with Postfix" off \
"Net"		"Other networking functions (dns...)" off \
"Extra"		"Extra packages (redis, nginx...)" off 2> $RESP


	cat $RESP | grep "Apache" >/dev/null
	if [[ "$?" == "0" ]]; then
		setup_apache
	fi

        cat $RESP | grep "MySQL" >/dev/null
        if [[ "$?" == "0" ]]; then
                setup_mysql
        fi

        cat $RESP | grep "Mail" >/dev/null
        if [[ "$?" == "0" ]]; then
                setup_mail
        fi

        cat $RESP | grep "Net" >/dev/null
        if [[ "$?" == "0" ]]; then
                setup_net
        fi

        cat $RESP | grep "Extra" >/dev/null
        if [[ "$?" == "0" ]]; then
                setup_extra
        fi


}


function setup_scripts {
	cd $INSTDIR/scripts	
	dialog --ascii-lines --title Scripts --backtitle "Select script install dir" --clear --dselect /root 20 40 2> $RESP
        if [[ "$?" != "0" ]]; then
                return
        fi
        DIR=$(cat $RESP)
        cp *.sh $DIR
}



function batch_mode {
	apt_up
	#install_packages base
	setup_ntp
	setup_hostname
	setup_dns
	setup_ssh_server
	setup_ssh_key
	setup_iptables
	config_functions
	setup_scripts
}


function interactive_mode {

	while true; do

	dialog  --ascii-lines --title Scripts --backtitle "Interactive Setup" --clear --title "Select operation" \
	        --menu "Select operation to run:" 20 50 12 \
	        "apt_up"			"Initial apt-get update/upgrade" \
        	"install_packages base"		"Install Base packages" \
	        "setup_ntp"			"NTP date Setup" \
        	"setup_hostname"		"Set Hostname" \
	        "setup_dns"			"DNS Setup" \
        	"setup_ssh_server"		"Set our sshd_config file" \
	        "setup_ssh_key"			"Set our idrsa.pub file" \
		"add_users"			"Add non privileged users" \
        	"setup_iptables"		"Set up firewall" \
	        "config_functions"		"Choose and setup server functions" \
        	"setup_scripts"			"Install some handy scripts" \
		"return"			"Go back / exit" 2> $RESP


	if [[ "$?" != "0" ]]; then
		return
	fi

	CHOICE=$(cat $RESP)
	$CHOICE

	done
}



###############################################################################
# Main


## Check if we are in an UBUNTU box
cat /etc/lsb-release | grep -i ubuntu > /dev/null
if [[ "$?" != "0" ]]; then
	echo "ERROR: This is not a UBUNTU Box !!!"
	echo "Using this script couldbreak you system"
	exit
fi

ID=$(id -u)
if [[ "$ID" != "0" ]]; then
	echo "ERROR: This script must be run as root"
	echo "... and you are not!"
	exit
fi

echo "Installing base mandatory packages..."

install_packages base

## select between interactive and batch mode

dialog  --ascii-lines --title Scripts --backtitle "System Setup Script" --clear --title "Seect mode" \
	--menu "Select operation mode:" 10 50 2 \
	"Batch"	"Runs all setup in a sequence" \
	"Interactive"	"Select each individual option" 2> $RESP

if [[ "$?" != "0" ]]; then
	exit
fi

cat $RESP | grep "Batch" > /dev/null
if [[ "$?" == "0" ]]; then
	batch_mode
else
	interactive_mode
fi

clear
