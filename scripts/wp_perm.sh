#!/bin/bash
#
#  Harden and LockDown WordPress Site files and folder permissions
#

DIR=$1
if [ "$DIR" == "" ]; then
	echo "You need to supply a Worpress Folder"
	exit
fi
if [ ! -d "$DIR" ]; then
	echo "can't find folder $DIR"
	exit
fi
if [ ! -d "$DIR/wp-admin" ]; then
	echo "$DIR seems not to be a wordpress root folder"
	exit
fi

# Set clean htaccess for uploaded files
cat > $DIR/wp-content/uploads/.htaccess <<EOF
# Kill PHP Execution
<Files *.php>
  deny from all
</Files>
<IfModule mod_php5.c>
  php_flag engine off
</IfModule>
EOF
chmod 640 $DIR/wp-content/uploads/.htaccess

## Lock permission
chown -R root:www-data $DIR
chmod -R u+rw $DIR
chmod -R g=r $DIR
find "$DIR" -type d -exec chmod ug+x {} \;
chmod -R o-rwx $DIR
# Allow uploads
chmod -R g+w $DIR/wp-content



