#!/bin/bash
#
#  CLONE DB 
#

LOCALDIR='/__BACKUP__'
MYSQLUSER=backup
MYSQLPASS=backup
OLD=$1
NEW=$2

cd $LOCALDIR

if [[ "OLD" == "" || "$NEW" == "" ]]; then

        # get a list of databases
        # databases=`mysql -u$MYSQLUSER -p$MYSQLPASS -e "SHOW DATABASES;" | tr -d "| " | grep -v Database | grep -v schema | grep -v mysql`
        mysql -e "SHOW DATABASES;" | tr -d "| " | grep -v Database | grep -v schema | grep -v mysql | column -t
        exit
fi

TMP=$(mktemp)
#dump
$LOCALDIR/drupaldb_bkp.sh $MYSQLUSER $MYSQLPASS $OLD $TMP
#restore
mysql -e "create database $NEW;"
mysql $NEW < $TMP

rm -Rf $TMP
