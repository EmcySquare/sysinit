#!/bin/bash
# add extra virtual host

#
# Config per
#

SITE=$(echo $1 | sed "s:www.::")
HOMY=$2

if [[ "$HOMY" == "" ]]; then
        HOMY=/var/www/$1
fi

LEVELS=$(echo $1 | tr "." "\n" | wc -l)
(( LEVELS > 3 )) &&  SSL=$1 ||SSL="ho-itala.it"

######################################
#
#  Create Virtual Host File
#

cat > new1 << EOF
#<VirtualHost *:80>
#        ServerName $SITE
#        ServerAlias www.$SITE
#
#        DocumentRoot $HOMY
#
#        <Directory $HOMY>
#                RewriteEngine On
#                RewriteCond %{SERVER_PORT} 80
#                RewriteCond %{REQUEST_URI} !^/.well-known
#                RewriteRule ^(.*)$ https://$SITE [R,L]
#        </Directory>
#
#        ServerSignature Off
#</VirtualHost>

<VirtualHost *:80>
## <VirtualHost *:443>
        ServerName $SITE
        ServerAlias www.$SITE
        ## DirectoryIndex  index.php




        DocumentRoot $HOMY
        <Directory $HOMY>
                AllowOverride All
                Options -Indexes
                Options +FollowSymLinks

                ## IP Filtering
                Order deny,allow
                Deny from all
                Allow from 127.0.0.1
                Allow from 192.168.1
                Allow from 192.168.2
                Allow from 212.239.25
                Allow from 213.215.202.99
        </Directory>

        ## Basic Auth
        #<Location />
        #   AuthType Basic
        #   AuthName $SITE
        #   Require valid-user
        #   AuthUserFile $HOMY/.htpasswd                              
        #   Order allow,deny
        #   Allow from 213.215.202.99
        #   Satisfy any
        #</Location>

        #SSLEngine On
        #SSLProtocol all -SSLv2 -SSLv3
        #SSLCipherSuite ALL:!DH:!EXPORT:!RC4:+HIGH:+MEDIUM:!LOW:!aNULL:!eNULL
        #### certificati del server
        #SSLCertificateFile /etc/letsencrypt/live/$SSL/cert.pem
        #SSLCertificateKeyFile /etc/letsencrypt/live/$SSL/privkey.pem
        #SSLCertificateChainFile /etc/letsencrypt/live/$SSL/fullchain.pem
        #SSLCACertificateFile /etc/letsencrypt/live/$SSL/fullchain.pem

        LogLevel error
        SetEnvIf Remote_Addr "127\.0\.0\.1" loopback 1
        CustomLog "|/usr/bin/rotatelogs /var/log/apache2/new_access_%Y-%m-%d.log 86400" combined env=!loopback
        ErrorLog "|/usr/bin/rotatelogs /var/log/apache2/new_error_%Y-%m-%d.log 86400"

        ServerSignature Off
</VirtualHost>
EOF

mv new1 /etc/apache2/sites-available/$SITE.conf


#######################################
#
# Create Folders
#

mkdir -p $HOMY
chmod -R git:www-data $HOMY
find $HOMY -type d -exec chmod g+s {} \;

#######################################
#
# Install robots
#

mv $HOMY/robots.txt $HOMY/robots.OLD 2>&1 >> /dev/null
cat > $HOMY/robots.txt <<EOF
  User-agent: *
  Disallow: /
  Disallow: /cgi-bin/
EOF

#######################################
#
# Permessi
#

chown -R www-data:www-data $HOMY
chmod -R 774 $HOMY


#######################################
#
# Fine Setup e restart Apache
#

cd /etc/apache2/sites-enabled
ln -s ../sites-available/$1.conf $1.conf

/etc/init.d/apache2 restart
