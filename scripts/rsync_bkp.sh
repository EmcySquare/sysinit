#!/bin/bash

BASEDIR="/mnt/BACKUPS/bkpwww_sync/"
REMOTEUSER="root"

#
# CONFDB= HOST;REMOTE_DIR;RSYNC_PARAMS
#
CONFDB="
192.168.1.3;/home/__BACKUPS__;
192.168.1.4;/__BACKUPS__;
192.168.1.5;/__BACKUPS__;
192.168.1.123;/mnt/drbd/__BACKUP__;--delete
"

function LinuxServerSync
{
		REMOTESERVER=$1
		REMOTEBKPDIR=$2
		RSYNCPARAMS=$3
		LOCALDIR=$BASEDIR$REMOTESERVER/
		mkdir -p $LOCALDIR
		rsync -avz $RSYNCPARAMS -e "ssh -p 2552 -oBatchMode=yes -oStrictHostKeyChecking=no" \
			$REMOTEUSER@$REMOTESERVER:$REMOTEBKPDIR  $LOCALDIR
}



for LINE in $CONFDB
do
		SERVER=$(echo $LINE | cut -d';' -f1)
		DIR=$(echo $LINE | cut -d';' -f2)
		PARAMS=$(echo $LINE | cut -d';' -f3)
		LinuxServerSync $SERVER $DIR $PARAMS
done
