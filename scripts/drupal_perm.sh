#!/bin/bash

## Go to right forlder
cd /var/www

## ownership
chown -R git:www-data *

## generic "blind" permissions
chmod -R u+rw,g+r,o-rwx *
find . -type d ! -perm -g=s  -exec chmod g+s {} \;

## UGC (sites/*/files)
# add write permission to those folders that DON'T have it (but need it)
find . -type d -regex "^.*/sites/[^/]*/files"  ! -perm -g=w -exec chmod -R g+w {} \;

# remove write permissions to those folders that DO have it (but must not)
find . -type d ! -regex "^.*/sites/[^/]*/files" -perm -g=w -exec chmod -R g-w {} \;
