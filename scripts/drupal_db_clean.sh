#!/bin/bash
MYSQLUSER=decoupler
MYSQLPASS=ScoppiaTore
SQL=$(mktemp -u)


function hunt {
        TMP1=$(mktemp -u)
        TMP2=$(mktemp -u)
        mysql -u$MYSQLUSER -p$MYSQLPASS -e "SELECT table_schema as 'DB', table_name AS 'Tables' FROM INFORMATION_SCHEMA.TABLES where table_name like '"$1"' INTO OUTFILE \""$TMP1"\" FIELDS TERMINATED BY ';' LINES TERMINATED BY '\n' ;"

        cat $TMP1 | tr ";" "." | tr -d "\"" > $TMP2
        while read line; do 
                echo "truncate table $line;" >> $SQL
        done < $TMP2

        rm -f $TMP1 $TMP2
}

hunt cache_%
hunt watchdog

mysql -u$MYSQLUSER -p$MYSQLPASS < $SQL
rm -f $TMP1 $TMP2
