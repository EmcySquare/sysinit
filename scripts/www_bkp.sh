#!/bin/bash
#
#  BACKUP /var/www FOLDER
#  AND
#  UPLOAD IT VIA FTP
#

LOCALDIR='/__BACKUP__'
DATE=$(date "+%F")
DUMPDIR=$LOCALDIR/www_dump
OLDSEC=$((60*60*24*5))

FTPUSER=
FTPPASS=
FTPSERV=

cd $LOCALDIR
mkdir -p $DUMPDIR/$DATE

## Delete older Backups (to save space)
for F in $(find $DUMPDIR -type d -maxdepth 1 -mindepth 1)
do
                  SEC=$(( $(date +"%s") - $(stat -c "%Y" $F) ))
                  if (( SEC > OLDSEC )); then
                                                  rm -Rf $F
                  fi
done

cd $DUMPDIR/$DATE
for D in $(find /var/www -type d -maxdepth 1 -mindepth 1)
do
        B=$(basename $D)
        cp -Raf $D $DUMPDIR/$DATE/$B
        tar -zcf  $DUMPDIR/$DATE/$B.tgz  $DUMPDIR/$DATE/$B
        rm -Rf  $DUMPDIR/$DATE/$B
done


cat > lftp.cmd <<EOF
lftp -u '$FTPUSER,$FTPPASS' $FTPSERV
lcd $DUMPDIR
mkdir web
cd web
mirror -R --delete-first
exit
EOF

lftp -f lftp.cmd

rm lftp.cmd
