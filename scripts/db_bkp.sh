#!/bin/bash
#
#  BAKCKUP all MYSQL DATABASES
#  AND
#  UPLOAD IT VIA FTP
#

LOCALDIR='/__BACKUP__'
DATE=$(date "+%F")
DUMPDIR=$LOCALDIR/dump
OLDSEC=$((60*60*24*3))

MYSQLUSER=
MYSQLPASS=

FTPUSER=
FTPPASS=
FTPSERV=


cd $LOCALDIR
mkdir -p $DUMPDIR/$DATE

## Delete older Backups (to save space)
for F in $(find $DUMPDIR -type d -maxdepth 1 -mindepth 1)
do
	  SEC=$(( $(date +"%s") - $(stat -c "%Y" $F) ))
	  if (( SEC > OLDSEC )); then
			  rm -Rf $F
	  fi
done

# get a list of databases
databases=`mysql -u$MYSQLUSER -p$MYSQLPASS -e "SHOW DATABASES;" | tr -d "| " | grep -v Database | grep -v schema`

# dump each database in turn
for db in $databases; do
  echo $db
  # NON-DRUPAL DB
  # mysqldump --force --opt -u$MYSQLUSER -p$MYSQLPASS --single-transaction --add-drop-database --routines --databases $db -r $DUMPDIR/$DATE/dump_$db.sql
  # DRUPAL DB
  $LOCALDIR/drupaldb_bkp.sh $MYSQLUSER $MYSQLPASS $db $DUMPDIR/$DATE/dump_$db.sql

  gzip -f $DUMPDIR/$DATE/dump_$db.sql
done


##################################################################################
#
# Backup to FTP
#
# cat > lftp.cmd <<EOF
# lftp -u '$FTPUSER,$FTPPASS' $FTPSERV 
# lcd $DUMPDIR
# mkdir db
# cd db
# mirror -R --delete-first
# exit
# EOF
# 
# lftp -f lftp.cmd
# rm lftp.cmd


##################################################################################
#
# Backup to Amazon S3
#
# cd $LOCALDIR
# date "+%F %T" > LastBackup.txt
# (time $LOCALDIR/mc mirror --force $LOCALDIR s3decoupler/decoupler-hoitalia-it/Database/) 2> LastBackup.txt
