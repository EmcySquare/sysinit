!#/bin/bash

USER=$1
PASS=$2
DEFAULT_HOME="/mnt/FTP"
HOME=$3


function usage ()
{
        echo " usage : $0 <user> <pass> </home>"
        exit
}

if [[ "$USER" == "" ]]; then
        usage
fi
if [[ "$PASS" == "" ]]; then
	PASS=$(echo $(date +%s,$USER) | md5sum | sha512sum | dd bs=12 count=1 2>/dev/null)
fi
if [[ "$HOME" == "" ]]; then
        HOME=$DEFAULT_HOME/$USER
fi

echo -e "Add : \n\
\t user: $USER \n\
\t pass: $PASS \n\
\t home: $HOME \n"

read -p "(Y/n)"
if [[ "$REPLY" != "Y" && "$REPLY" != "y" && "$REPLY" != "" ]]; then
        exit
fi

mkdir -p $HOME
useradd -d $HOME -s /dev/null $USER 2>&1
#usermod -p $PASS $USER
echo "$USER:$PASS" | chpasswd
usermod -a -G ftp $USER
chown -R $USER $HOME
chgrp -R ftp $HOME


