USER=$1
PASSWORD=$2
DATABASE=$3
DB_FILE=$4
HOST=localhost


SKIP=$(mysql --user=${USER} --password=${PASSWORD} -h ${HOST} -e "use $DATABASE; show tables like 'cache%'" | tr -d "| " | grep -e "^cache")


IGNORED_TABLES_STRING=''
for TABLE in $(echo "${SKIP}")
do :
   IGNORED_TABLES_STRING+=" --ignore-table=${DATABASE}.${TABLE}"
done

TMP1=$(mktemp)
TMP2=$(mktemp)
echo "Dump structure"
mysqldump --host=${HOST} --user=${USER} --password=${PASSWORD} --single-transaction --no-data ${DATABASE} -r $TMP1

echo "Dump content"
mysqldump --host=${HOST} --user=${USER} --password=${PASSWORD} --single-transaction --routines ${DATABASE} ${IGNORED_TABLES_STRING} -r $TMP2

cat $TMP1 $TMP2 > ${DB_FILE}
rm -f $TMP1 $TMP2